import { tokens, EVM_REVERT, ETHER_ADDRESS, ether} from './helpers'

const Token = artifacts.require('./Token')
const Platform = artifacts.require('./Platform')

const wait = (seconds) => {
	const milliseconds = seconds * 1000
	return new Promise(resolve => setTimeout(resolve,milliseconds))
	}

require('chai')
	.use(require('chai-as-promised'))
	.should()





contract('Platform', ([deployer,user1,user2,user3,user4,user5]) => {
	let token
	let platform 
	const feePercent = 3


	beforeEach( async ()=> {

		token = await Token.new()

		token.transfer(user1, tokens(100), { from: deployer})

		platform = await Platform.new(feePercent)


	})

	describe('deployment', ()=> {
		it('tracks the feePercent', async ()=>{

			const result = await platform.feePercent()
			result.toString().should.equal(feePercent.toString())

		})

		it('check if user1 has tokens', async ()=>{
			let balanceOf

			balanceOf = await token.balanceOf(deployer)
			balanceOf.toString().should.equal(tokens(999900).toString())
			console.log(balanceOf.toString(), 'deployer balance updated')

			balanceOf = await token.balanceOf(user1)
			balanceOf.toString().should.equal(tokens(100).toString())
			console.log(balanceOf.toString(),'user1 balance updated')
		})
	})

	describe('fallback', async ()=> {
		it('reverts when ether is sent directly', async ()=> {
			await platform.sendTransaction( { value:1 , from: user1}).should.be.rejectedWith(EVM_REVERT)
		})
	})

	describe('depositing Ether', async ()=> {
		let amount
		let result

		beforeEach( async ()=> {
			amount = ether(0.01)
			result = await platform.depositEther( { value: amount, from: user1})
		})

		it('tracks ether deposit', async ()=> {
			const balance = await platform.tokens(ETHER_ADDRESS, user1)
			balance.toString().should.equal(amount.toString())
		})

		it('emits an ether Deposit event', async ()=>{
			const log = result.logs[0]
			log.event.should.eq('Deposit')
			const event = log.args
			event.token.should.equal(ETHER_ADDRESS, 'ETHER address is correct')
			event.user.should.equal(user1, 'user address is correct')
			event.amount.toString().should.equal(amount.toString(), 'amount is correct')
			event.balance.toString().should.equal(amount.toString(), 'balance is correct')
			// console.log(log)
			// console.log(event)
		})
	})

	describe('withdrawing Ether', async () => {
		let amount
		let result

		beforeEach( async () => {
			amount = ether(0.01)
			result = await platform.depositEther( {value: amount, from: user1} )

		})

		describe('success', async ()=>{
			beforeEach( async () => {

				result = await platform.withdrawEther(amount, {from: user1})

			})

			it('withdraws ether funds', async ()=>{

				const balance = await platform.tokens(ETHER_ADDRESS, user1)
				balance.toString().should.equal('0')
				console.log(balance.toString(), 'ether balance after trying withdraw')
			})

			it('emits a withdraw Event', async ()=> {
				const log = result.logs[0]
				log.event.should.eq('Withdraw')
				const event = log.args
				event.token.should.equal(ETHER_ADDRESS, 'ETHER address is correct')
				event.user.should.equal(user1, 'user address is correct')
				event.amount.toString().should.equal(amount.toString(), 'amount is correct')
				event.balance.toString().should.equal('0', 'balance is correct')
				// console.log(log)
				// console.log(event)
			})
		})

		describe('failure', async ()=> {
			it('rejects withdraw with insufficient funds', async () => {
				await platform.withdrawEther(ether(100)).should.be.rejectedWith(EVM_REVERT)
			})

			it('tries to withdraw while a call started', async ()=> {
				await platform.startCall(user2, ether(0.0001), 3600, {from: user1})
				await platform.withdrawEther(amount, {from: user1}).should.be.rejectedWith(EVM_REVERT)
			})
			// it('rejects withdraw if timelocked works', async ()=> {
			// 	const secondsBooked = 100
			// 	await platform.withdrawEther(ether(0.1)).should.be.rejectedWith(EVM_REVERT)

			// })
		})

	})

	describe('checkin balances', async () => {
			beforeEach(async ()=> {
				await platform.depositEther({from: user1, value: ether(0.01)})
			})

			it('checks user token or ether balances', async () => {
				const result = await platform.balanceOf(ETHER_ADDRESS, user1)
				result.toString().should.equal(ether(0.01).toString())
			})


	})

	describe('starting call', async () => {
		let result
		let amount
		beforeEach(async ()=> {
			await platform.depositEther({from:user1, value: ether(0.1)})
			result = await platform.startCall(user2, ether(0.001), 3600, {from: user1})
				
			})


		describe('success', async () => {
			
			

			it('checks if totalPrice and callCount are correct', async () => {
			amount = await platform.totalPrice()
			//console.log(amount.toString())
			amount.toString().should.equal(ether(0.06).toString(),'totalPrice is correct')
			amount = await platform.callCount()
			amount.toString().should.equal('1', 'callCount is correct')
			})

			it('tracks the newly created call, verifies it happened', async () => {
			const callCount = await platform.callCount()
			callCount.toString().should.equal('1')
			const call = await platform.calls('1')
			call.id.toString().should.equal('1', 'id is correct')
			call.caller.should.equal(user1, 'caller is correct')
			call.callee.should.equal(user2, 'calle is correct')
			call.timestamp.toString().length.should.be.at.least(1,'timestamp is present')
			//console.log(call.timestamp.toString())
			})

			it('emit a StartCall event', async ()=> {
			const log =  result.logs[0]
			log.event.should.eq('StartCall')
			const event = log.args
			event.id.toString().should.equal('1', 'callCount is correct')
			event.caller.should.equal(user1, 'caller is correct')
			event.callee.should.equal(user2, 'callee is correct')
			event.timestamp.toString().length.should.at.least(1, 'timestamp is present')
			// console.log(log)
			//console.log(event)
			
			})
		
		})

		describe('failure', async () => {
			beforeEach(async ()=> {
				await platform.depositEther({from:user1, value: ether(0.1)})	
			})

			it('rejects if user has insufficient balance', async ()=> {

				await platform.startCall(user2, ether(1), 3600, {from: user1}).should.be.rejectedWith(EVM_REVERT)
			})
		})


	

	})


	describe('ending a call', async () => {
		let result 
		let amount
		let balance
		
		

		describe('success', async () => {
			beforeEach(async () => {
			
			await platform.depositEther({from:user1, value: ether(0.2)})
			await platform.startCall(user2, ether(0.01), 120, {from: user1})
			
			await wait(10) 

			result = await platform.endCall('1', {from:user1})
		
		
			})

	  		
	  		it('calculate the seconds payable through timestamp', async () => {
	  				
	  			amount = await platform._secondsToPay()
	  			amount.toString().should.equal('10', 'seconds to pay are correct')
	  			
			})

			it('updates the callEnded mapping (ended calls)', async ()=> {
							
				const callEnded = await platform.callEnded(1)
				const verify = await platform.verify(user1)
				callEnded.should.equal(true)
				verify._startCall.should.equal(false)
			})

			it('emits a EndCall event', async () => {
				const log = result.logs[0]
				log.event.should.equal('EndCall')
				const event = log.args
				event.id.toString().should.equal('1', 'id is correct')
				event.caller.should.equal(user1, 'caller is correct')
				event.callee.should.equal(user2, 'callee is correct')
				event.timestamp.toString().length.should.be.at.least(1,'timestamp is present')
				//console.log(event)
			})

			it('withdraws ether left in the balance', async()=> {

				balance = await platform.balanceOf(ETHER_ADDRESS, user1)
				await platform.withdrawEther(balance, {from:user1})
				balance = await platform.balanceOf(ETHER_ADDRESS, user1)
				balance.toString().should.equal('0', 'it withdraws all ether left')



			})
		})
	})
			
	describe('execute the payment function', async () => {
				
				let result
				let amount
				let balance

				it('checks balances and fees', async () => {

				result = await platform.depositEther({from:user3, value: ether(0.02)})
				
				
				result = await platform.startCall(user4, ether(0.01), 120, {from: user3})
				amount = await platform.totalPrice()
				
							
				await wait(15) 

				result = await platform.endCall('1', {from:user3})
				
				
				amount = await platform._secondsToPay()
		
				balance = await platform.balanceOf(ETHER_ADDRESS, user3)
				balance.toString().should.equal(ether(0.0175).toString(),'caller balance is correct')

				balance = await platform.balanceOf(ETHER_ADDRESS, user4)
				balance.toString().should.equal(ether(0.002425).toString(), 'callee balance is correct')
				
				})

				it('emits a TradeClosed event', async()=> {
					let result

					await platform.depositEther({from:user1, value: ether(0.2)})
					await platform.startCall(user2, ether(0.01), 120, {from: user1})
			
					await wait(1) 

					result = await platform.endCall('1', {from:user1})

					await platform.end
					const log = result.logs[1]//segundo log de la funcion endCall (0 es EndCall, 1 es TradeClosed)
					log.event.should.equal('TradeClosed')
					const event = log.args
					event.id.toString().should.equal('1', 'id is correct')
					event.caller.should.equal(user1, 'caller is correct')
					event.callee.should.equal(user2, 'callee is correct')
					event.timestamp.toString().length.should.be.at.least(1,'timestamp is present')
					event.payment.toString().should.equal('166666666666666', 'payment is correct')
					event.fee.toString().should.equal('5000000000000','fee is correct')
					//console.log(event)

				})

	})

	describe(' Donation Case', async ()=> {
		let result
		let balance
		it('donates 2 transaction gas amount to the beneficiary', async() => {
			await platform.depositEther({from:user3, value: ether(0.02)})
				
				
			await platform.startCall(user4, ether(0.01), 120, {from: user3})
			await platform.totalPrice()
				
							
			await wait(3) 

			await platform.endCall('1', {from:user3})

			await platform.transfer(user4)
		})

		it('emits a Donation event', async ()=> {
			let balance4t
			let balance4
			let balance1

			await platform.depositEther({from:user3, value: ether(0.02)})
				
				
			await platform.startCall(user4, ether(0.01), 120, {from: user3})
			await platform.totalPrice()
				
							
			await wait(3) 

			await platform.endCall('1', {from:user3})

			balance4t = await platform.balanceOf(ETHER_ADDRESS,user4) 

			balance4 = await platform.balanceEther(user4)
			console.log('beneficiary balances before Donation ', balance4t.toString(), balance4.toString())
			balance1 = await platform.balanceEther(user1)
			console.log(balance1.toString())

			result = await platform.transfer(user4, {from:user3})


			const log = result.logs[0]
			log.event.should.equal('Donation')
			const event = log.args
			event.id.toString().should.equal('1', 'id is correct')
			event.donator.should.be.equal(user3)
			event.beneficiary.should.be.equal(user4)
			event.timestamp.toString().length.should.be.at.least(1,'timestamp is present')
			balance4t = await platform.balanceOf(ETHER_ADDRESS,user4)

			balance4 = await platform.balanceEther(user4)
			console.log('beneficiary balances after Donation ', balance4t.toString(), balance4.toString())
			balance1 = await platform.balanceEther(user1)
			console.log(balance1.toString())
		})
	})

	
	
	
	
})