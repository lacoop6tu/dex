import { tokens, EVM_REVERT, ETHER_ADDRESS, ether} from './helpers'

const Token = artifacts.require('./Token')
const Dividend = artifacts.require('./Dividend')

const wait = (seconds) => {
	const milliseconds = seconds * 1000
	return new Promise(resolve => setTimeout(resolve,milliseconds))
	}

require('chai')
	.use(require('chai-as-promised'))
	.should()





contract('Dividend', ([deployer,user1,user2]) => {
	let token
	let platform 
	const feePercent = 3


	beforeEach( async ()=> {

		token = await Token.new()

		token.transfer(user1, tokens(10000), { from: deployer})

		token.transfer(user2, tokens(10000), { from: deployer})

		platform = await Dividend.new(feePercent)


	})

	describe('deployment', ()=> {
		it('tracks the feePercent', async ()=>{

			const result = await platform.feePercent()
			result.toString().should.equal(feePercent.toString())

		})

		it('check if user1 has tokens', async ()=>{
			let balanceOf

			balanceOf = await token.balanceOf(deployer)
			balanceOf.toString().should.equal(tokens(980000).toString())
			console.log(balanceOf.toString(), 'deployer balance updated')

			balanceOf = await token.balanceOf(user1)
			balanceOf.toString().should.equal(tokens(10000).toString())
			console.log(balanceOf.toString(),'user1 balance updated')
		})

		it('deposits some tokens from user1', async ()=> {

			describe('depositing tokens', ()=> {
			let result
			let amount  

		
		
			describe('success', ()=> {
				
				beforeEach(async ()=> {
			amount= tokens(10)
			await token.approve(platform.address , amount , {from: user1})
			result = await platform.depositToken(token.address, amount, {from: user1})
			})

			// it('tracks the token deposit', async ()=> {
			// let balance
			// //check exchange token balance
			// balance = await token.balanceOf(platform.address)
			// balance.toString().should.equal(amount.toString())
			// // check user1 tokens on exchange
			// balance = await platform.tokens(token.address, user1)
			// balance.toString().should.equal(amount.toString())

			// })

			it('emits a Deposit event', async () => {
			let log = await result.logs[0]
			log.event.should.eq('DepositT')
			let event = log.args
			event.count.toString().should.eq('1', 'count is correct')
			event.token.should.eq(token.address, 'token address is correct')
			event.user.should.eq(user1, 'user address is correct')
			event.amount.toString().should.eq(amount.toString(),' amount is correct')
			event.balance.toString().should.eq('20000000000000000000',' balance is correct')
			console.log('deposit event', event)

			amount = tokens(10)
			await token.approve(platform.address, amount, {from: user1})
			await platform.depositToken(token.address, amount, {from: user1})
			
			//withdraw tokens
			result = await platform.withdrawToken(token.address, amount, {from: user1})
			 log = await result.logs[0]
			log.event.should.eq('WithdrawT')
			 event = log.args
			console.log(event, 'withdraw proof')


			})	
			})
		})
		})
	})
})