// TESTING TRANSACTION AND FAILURES

import { tokens, EVM_REVERT, ETHER_ADDRESS, ether} from './helpers'

const Token = artifacts.require('./Token')
const Platform = artifacts.require('./Platform')

const wait = (seconds) => {
	const milliseconds = seconds * 1000
	return new Promise(resolve => setTimeout(resolve,milliseconds))
	}

require('chai')
	.use(require('chai-as-promised'))
	.should()





contract('Platform', ([deployer,user1,user2,user3,user4]) => {
	let token
	let platform 
	const feePercent = 3


	beforeEach( async ()=> {

		token = await Token.new()

		token.transfer(user1, tokens(100), { from: deployer})

		platform = await Platform.new(feePercent)


	})
	describe('ONE startCall has been triggered but not endCall', async () => {
	
		describe('failure', async ()=> {
			
			beforeEach(async () => {
			
			await platform.depositEther({from:user1, value: ether(0.5)})
			await platform.startCall(user2, ether(0.00001), 120, {from: user1})
			})
				it('callee user tries to end the call', async ()=> {
				await platform.endCall('1', {from:user2}).should.be.rejectedWith(EVM_REVERT);
				})

				it('external user tries to end the call', async ()=> {
				await platform.endCall('1', {from:user3}).should.be.rejectedWith(EVM_REVERT);
				})

				it('tries to withdraw', async ()=> {
				await platform.withdrawEther(ether(0.01), {from:user1}).should.be.rejectedWith(EVM_REVERT)
				})

				it('try to withdraw with one call closed and the opened a new one', async ()=> {
				await platform.endCall('1', {from:user1})
				await platform.startCall(user2, ether(0.00001), 120, {from: user1})
				await platform.endCall('2', {from:user1})
				await platform.startCall(user2, ether(0.00001), 120, {from: user1})
				await platform.withdrawEther(ether(0.01), {from:user1}).should.be.rejectedWith(EVM_REVERT)


				})

				it('tries to open a second call while another is not ended', async ()=> {
				await platform.startCall(user2, ether(0.00001), 120, {from: user1}).should.be.rejectedWith(EVM_REVERT)

				})

				it('callee or different user tries to end the call', async ()=> {
				await platform.endCall('1', {from:user3}).should.be.rejectedWith(EVM_REVERT)
				})
		})
	})



})