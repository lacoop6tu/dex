import { tokens, EVM_REVERT } from './helpers' 

const Token = artifacts.require('./Token')

require('chai')
	.use(require('chai-as-promised'))
	.should()





contract('Token', ([deployer,receiver, exchange]) => {
	const name = 'Tusi token'
	const symbol = 'TUSI'
	const decimals = 18
	const totalSupply = tokens(1000000).toString()
	let token

	beforeEach(async ()=> {
	
	token = await Token.new()
	})

	describe('deployment', () => {
		it('tracks the name', async ()=>  {
			
			const result = await token.name()
			result.should.equal(name)
		})
		it('tracks the token symbol', async()=> {
			
			const result = await token.symbol()
			result.should.equal(symbol)
		})
		it('tracks the decimals', async()=> {
			
			const result = await token.decimals()
			result.toNumber().should.equal(decimals)
		})
		it('tracks the total supply', async()=> {
			
			const result = await token.totalSupply()
			result.toString().should.equal(totalSupply.toString())
		})
		it('assigns all tokens to deployer', async() => {

			const result = await token.balanceOf(deployer)
			result.toString().should.equal(totalSupply.toString())


		})
	})
		
	describe ('send tokens', ()=> {
			let amount
			let result

		describe('success', ()=> {

			beforeEach(async ()=> {
			amount = tokens(100)
			result = await token.transfer(receiver,amount, {from: deployer })
			})

		it('transfers tokens', async()=> {
			let balanceOf
			//balance before transfer
			//balanceOf = await token.balanceOf(deployer)
			//console.log('deployer balance before transfer', balanceOf.toString())
			//balanceOf = await token.balanceOf(receiver)
			//vsconsole.log('receiver balance before transfer', balanceOf.toString())

			// balance after transfer
			balanceOf = await token.balanceOf(deployer)
			balanceOf.toString().should.equal(tokens(999900).toString())
			console.log('deployer balance after transfer', balanceOf.toString())
			balanceOf = await token.balanceOf(receiver)
			balanceOf.toString().should.equal(tokens(100).toString())
			console.log('receiver balance after transfer', balanceOf.toString())

		})
		it('emits a transfer event', async () => {
			const log = result.logs[0]
			log.event.should.eq('Transfer')
			const event = log.args
			event.from.toString().should.eq(deployer, 'from is correct')
			event.to.toString().should.eq(receiver, 'to is correct')
			event.value.toString().should.eq(amount.toString(),' amount is correct')
		})	


		})	
		
		describe('failure', ()=> {
			it('rejects the transaction', async ()=> {
				let invalidAmount 
				invalidAmount = tokens(100000000)
				await token.transfer(receiver, invalidAmount, {from:deployer}).should.be.rejectedWith(EVM_REVERT);

				invalidAmount = tokens(10)
				await token.transfer(deployer, invalidAmount, {from:receiver}).should.be.rejectedWith(EVM_REVERT);
			})

			it('rejects invalid recipients', async ()=> {
				await token.transfer(0x0, amount, {from:deployer}).should.be.rejected;
			})

		})
	})	

	describe ('approving tokens', ()=> {
		let amount
		let result 

		beforeEach(async ()=> {
			amount = tokens(100)
			result = await token.approve(exchange, amount, {from: deployer})
		})

		describe('success', ()=> {
			it('allocates an allowance for token spending on an exchange', async()=> {
				const allowance = await token.allowance(deployer, exchange)
				allowance.toString().should.eq(amount.toString())

			})

			it('emits an approval event', async () => {
			const log = result.logs[0]
			log.event.should.eq('Approval')
			const event = log.args
			event.owner.toString().should.eq(deployer, 'owner is correct')
			event.spender.toString().should.eq(exchange, 'spender is correct')
			event.value.toString().should.eq(amount.toString(),' amount is correct')
			})	
			it('rejects invalid recipients', async ()=> {
				await token.approve(0x0, amount, {from:deployer}).should.be.rejected;
			})
		})
	})


	describe ('delegated token transfers', ()=> {
			let amount
			let result

			beforeEach(async ()=> {
			amount = tokens(100)
			await token.approve(exchange,amount, { from: deployer})	
			})

		describe('success', ()=> {

			beforeEach(async ()=> {
			
			result = await token.transferFrom(deployer,receiver,amount, {from: exchange })
			})

		it('transfers tokens', async()=> {
			let balanceOf
			//balance before transfer
			//balanceOf = await token.balanceOf(deployer)
			//console.log('deployer balance before transfer', balanceOf.toString())
			//balanceOf = await token.balanceOf(receiver)
			//vsconsole.log('receiver balance before transfer', balanceOf.toString())

			// balance after transfer
			balanceOf = await token.balanceOf(deployer)
			balanceOf.toString().should.equal(tokens(999900).toString())
			console.log('deployer balance after transfer', balanceOf.toString())
			balanceOf = await token.balanceOf(receiver)
			balanceOf.toString().should.equal(tokens(100).toString())
			console.log('receiver balance after transfer', balanceOf.toString())

		})

		it('reset the allowance', async()=> {
				const allowance = await token.allowance(deployer, exchange)
				allowance.toString().should.eq('0')
			})

		it('emits a transfer event', async () => {
			const log = result.logs[0]
			log.event.should.eq('Transfer')
			const event = log.args
			event.from.toString().should.eq(deployer, 'from is correct')
			event.to.toString().should.eq(receiver, 'to is correct')
			event.value.toString().should.eq(amount.toString(),' amount is correct')
		})	


		})	
		

		describe('failure', ()=> {

			 it('rejects the transaction', async ()=> {
			 	let invalidAmount 
			 	invalidAmount = tokens(100000000)
			 	await token.transferFrom(deployer,receiver, invalidAmount, {from:exchange}).should.be.rejectedWith(EVM_REVERT);
			 })
			 it('rejects invalid recipients', async ()=> {
			 	await token.transferFrom(deployer, 0x0, amount, {from:exchange}).should.be.rejected;
			 })

		})
	})	
})