import React, { Component } from 'react'
import './App.css'
import Navbar from './Navbar'
import Content from './Content'
import { connect } from 'react-redux'
import {
  loadWeb3, 
  loadAccount, 
  loadToken, 
  loadExchange,
}  from '../store/interactions'
import { contractsLoadedSelector } from '../store/selectors' 


class App extends Component {
  // componentWillMount() {
  //   this.loadBlockchainData(this.props.dispatch)

  // }

  async loadBlockchainData(dispatch){

    const web3 = loadWeb3(dispatch)
    await web3.eth.net.getNetworkType()
    const networkId = await web3.eth.net.getId()
    await loadAccount(web3, dispatch)
     //fetch from Token.json abi and address, both needed for smart contract interaction
    //console.log('abi' , Token.abi)
    //console.log('address', Token.networks[networkId].address)
    const token = await loadToken(web3, networkId, dispatch)
    const exchange = await loadExchange(web3, networkId, dispatch)
    if(!token) {
      window.alert('Token Smart Contract not detected on the current network.Please select another network with Metamask')
      return
    }
    if(!exchange){
      window.alert('Exchange Smart Contract not detected on the current network.Please select another network with Metamask')
      return
    }
    
    
    

  
    
  }
  render() {
    //console.log(this.props.account)
    return (
      <div>
          <Navbar />
          { this.props.contractsLoaded ? <Content /> : <div className="content"></div>}
      </div> 

    );
  }
}


function mapStateToProps (state){
  
  return {
    contractsLoaded : contractsLoadedSelector(state)
      //TODO: fill me in..
  }
}

export default connect (mapStateToProps)(App)


