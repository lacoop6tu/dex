pragma solidity ^0.5.0;

import "./Token.sol";
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
// TODO 
//Set a Fee Account
//Deposit Ether
//Withdraw Ether
//Deposit Token
//Withdraw Token
//Check Balances
//Make Orders
//Cancel Orders
//Fill Orders
//Charge Fees

contract Exchange {
	using SafeMath for uint;

	address public feeAccount;
	uint256 public feePercent;
	address constant ETHER = address(0); // store ether in tokens mapping with blank address
	mapping(address => mapping(address => uint256)) public tokens; 
	mapping(uint256 => _Order) public orders;
	uint256 public orderCount;
	mapping(uint256 => bool ) public orderCancelled;
	mapping(uint256 => bool ) public orderFilled;

	event Deposit(address token, address user, uint256 amount, uint256 balance);
	event Withdraw(address token, address user, uint256 amount, uint256 balance);
	event Order(
		uint256 id,
		address user,
		address tokenGet,
		uint256 amountGet,
		address tokenGive,
		uint256 amountGive,
		uint256 timestamp
		);
	event Cancel(
		uint256 id,
		address user,
		address tokenGet,
		uint256 amountGet,
		address tokenGive,
		uint256 amountGive,
		uint256 timestamp
		);
	event Trade (
		uint256 id,
		address user,
		address tokenGet,
		uint256 amountGet,
		address tokenGive,
		uint256 amountGive,
		address userfill,
		uint256 timestamp
		);
	
	struct _Order{
		uint256 id;
		address user;
		address tokenGet;
		uint256 amountGet;
		address tokenGive;
		uint256 amountGive;
		uint256 timestamp;

	}
	constructor(address _feeAccount, uint256 _feePercent) public {
		feeAccount = _feeAccount;
		feePercent = _feePercent;


	}

	function() external {
		revert();
	}

	function depositEther() payable public{
		tokens[ETHER][msg.sender] = tokens[ETHER][msg.sender].add(msg.value);

		emit Deposit(ETHER, msg.sender, msg.value, tokens[ETHER][msg.sender]);
	}
	
	function withdrawEther(uint256 _amount) public {
		require(tokens[ETHER][msg.sender] >= _amount);
		tokens[ETHER][msg.sender] = tokens[ETHER][msg.sender].sub(_amount);
		msg.sender.transfer(_amount);
		emit Withdraw(ETHER,  msg.sender, _amount, tokens[ETHER][msg.sender] );

	}	

	function depositToken (address _token, uint256 _amount) public {
		require(_token != ETHER);
		require(Token(_token).transferFrom(msg.sender, address(this), _amount));
		
		tokens[_token][msg.sender] = tokens[_token][msg.sender].add(_amount);

		emit Deposit(_token, msg.sender, _amount, tokens[_token][msg.sender]);
	}

	function withdrawToken (address _token, uint256 _amount) public {
		require (_token != ETHER);
		require (tokens[_token][msg.sender] >= _amount);
		tokens[_token][msg.sender] = tokens[_token][msg.sender].sub(_amount);
		require (Token(_token).transfer(msg.sender, _amount));
		emit Withdraw (_token, msg.sender, _amount, tokens[_token][msg.sender]);
	}

	function balanceOf (address _token, address _user) public view returns (uint256){

		return tokens[_token][_user];
	}
	
	function makeOrder(address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive) public {
			orderCount = orderCount.add(1);

			orders[orderCount]= _Order(orderCount, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, now);
			emit Order (orderCount, msg.sender, _tokenGet, _amountGet, _tokenGive, _amountGive, now);

	}
	function cancelOrder(uint256 _id) public {
			_Order storage _order = orders[_id];
			require(address(_order.user) == msg.sender);
			require(_order.id == _id);

			orderCancelled[_id] = true;
			emit Cancel (_order.id, _order.user, _order.tokenGet, _order.amountGet, _order.tokenGive, _order.amountGive, now);


	}

	function fillOrder(uint256 _id) public {
		require (_id > 0 && _id <= orderCount); // require order id is valid: greater than zero and less than orderCount (orderCount is the only way to know how many orders exist as mapping can't count)
		require(!orderFilled[_id]); //require it has not been filled already	
		require(!orderCancelled[_id]); // require it has not been cancelled.
		//fetch the order
		_Order storage _order = orders[_id];  // _Order (type of data/struct), _order new variable mapped from orders function (through the id)
		// in order to trade we are going to create an internal function _trade
		_trade(_order.id, _order.user, _order.tokenGet, _order.amountGet, _order.tokenGive, _order.amountGive);
		// mark order as filled
		orderFilled[_order.id] = true;
		
	}

	function _trade(uint256 _orderId, address _user, address _tokenGet, uint256 _amountGet, address _tokenGive, uint256 _amountGive  ) internal {
			//fees paid by the user who fills the order, msg.sender
			//fee deducted from amountGet
			uint256 _feeAmount = _amountGive.mul(feePercent).div(100);

			
			//execute trade and charge fees , probablemente está equivocado Gregory. la fee se quita de msg.sender pero desde el amountGive

			tokens[_tokenGet][msg.sender] = tokens[_tokenGet][msg.sender].sub(_amountGet);
			tokens[_tokenGet][_user] = tokens[_tokenGet][_user].add(_amountGet);
			tokens[_tokenGive][feeAccount] = tokens[_tokenGive][feeAccount].add(_feeAmount);
			tokens[_tokenGive][_user] = tokens[_tokenGive][_user].sub(_amountGive);
			tokens[_tokenGive][msg.sender] = tokens[_tokenGive][msg.sender].add(_amountGive).sub(_feeAmount);
		
		// emit trade event
			emit Trade( _orderId, _user, _tokenGet, _amountGet, _tokenGive, _amountGive, msg.sender, now);
	}

}