const Token = artifacts.require("Token")
const Platform = artifacts.require("Platform")

//utils
const ETHER_ADDRESS= '0x0000000000000000000000000000000000000000' //Ether token deposit address
const ether = (n) =>{
	return new web3.utils.BN(
		web3.utils.toWei(n.toString(), 'ether')
		)
	}
	

const tokens = (n) => ether(n)

const wait = (seconds) => {
	const milliseconds = seconds * 1000
	return new Promise(resolve => setTimeout(resolve,milliseconds))
}



module.exports = async function(callback) {

	try {

		const accounts = await web3.eth.getAccounts()

		//fetch the deployed token address
		const token = await Token.deployed()
		console.log('Token fetched', token.address)
		//fetch the deployed exchange address
		const platform = await Platform.deployed()
		console.log('Platform fetched', platform.address)

		//give tokens to account[1]
		const sender = accounts[0]
		const receiver = accounts[1]

		let amount = web3.utils.toWei('10000', 'ether')// 10,000 tokens

		await token.transfer(receiver, amount, {from: sender})
		console.log( "Transferred" +amount +"tokens from"+ sender +"to" +receiver)

		// set up exchange users
		const user1 = accounts[0]
		const user2 = accounts[1]


		//user1 deposits Ether
		amount = 1
		await platform.depositEther({from: user1 , value: ether(amount)})
		console.log("Deposited " +amount+ "Ether from "+user1)

		//user2 approves Tokens
		amount = 10000
		await token.approve(platform.address , tokens(amount), {from:user2})
		console.log("Approved "+amount+ "tokens from "+user2)

		// //user2 deposit Tokens
		// await platform.depositToken(token.address, tokens(amount), {from:user2})
		// console.log("Deposited " +amount+ "Ether from "+user2)

		//user2 deposit Ether
		amount= 2
		await platform.depositEther({from: user2 , value: ether(amount)})
		console.log("Deposited " +amount+ "Ether from "+user2)

		//user1 make an order
		let result
		let orderId
		result = await platform.startCall(user2, ether(0.001), 120, {from: user1})
		console.log("Made Call from "+user1)



		// duration of the call
		await wait(100)

		//user1 ends a call
		orderId = result.logs[0].args.id
		await platform.endCall(orderId, {from:user1})
		console.log("Call ended by "+user1)

	
		

		// wait 1 second (due to timestamp, 1 second is the minimum unit of account)
		await wait(1)

		//user1 makes an order
		result = await platform.startCall(user2, ether(0.01), 120, {from: user1})
		console.log("Made Call from "+user1)



		// duration of the call
		await wait(100)

		//user1 ends a call
		orderId = result.logs[0].args.id
		await platform.endCall(orderId, {from:user1})
		console.log("Call ended by "+user1)
		

		// wait 1 second (due to timestamp, 1 second is the minimum unit of account)
		await wait(1)

		result = await platform.startCall(user1, ether(0.01), 120, {from: user2})
		console.log("Made Call from "+user2)



		// duration of the call
		await wait(60)

		//user1 ends a call
		orderId = result.logs[0].args.id
		await platform.endCall(orderId, {from:user2})
		console.log("Call ended by "+user2)




	}
	catch(error){
		console.log(error)
	}

	callback()
}