const Token = artifacts.require("Token");
const Exchange = artifacts.require("Exchange")

module.exports = async function(deployer) {
  const accounts = await web3.eth.getAccounts()

  await deployer.deploy(Token);
  // we are going to decided account 0 is going to be the fee account
  const feeAccount = accounts[0]
  const feePercent = 10

  await deployer.deploy(Exchange, feeAccount, feePercent) //first is the name of the contract, the others are the elements in the constructor function of the contract
  
};